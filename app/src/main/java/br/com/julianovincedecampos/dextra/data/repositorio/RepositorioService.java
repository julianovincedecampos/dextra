package br.com.julianovincedecampos.dextra.data.repositorio;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import br.com.julianovincedecampos.dextra.pojo.Repositories;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Juliano Vince on 29/05/2017.
 */
public interface RepositorioService {
    @GET("/search/repositories?")
    Observable<Repositorio> getRepositorios(@Query("q") String q);

    @GET("/search/repositories?")
    Observable<Repositorio> getRepositorios(@Query("q") int page);

    final class Repositorio implements Serializable {
        @SerializedName("items")
        public ArrayList<Repositories> repositorios;
    }
}
