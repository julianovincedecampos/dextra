package br.com.julianovincedecampos.dextra.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Juliano Vince on 31/05/2017.
 */

public class Repositories implements Parcelable {

    private String id;

    private String name;

    private String full_name;

    private String description;

    private Owner owner;

    public Repositories() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Repositories>
            CREATOR = new Parcelable.Creator<Repositories>() {

        public Repositories createFromParcel(Parcel in) {
            return new Repositories(in);
        }

        public Repositories[] newArray(int size) {
            return new Repositories[size];
        }
    };


    public Repositories(Parcel parcel) {
        this.id = parcel.readString();
        this.name = parcel.readString();
        this.full_name = parcel.readString();
        this.description = parcel.readString();
        this.owner = parcel.readParcelable(Owner.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(full_name);
        parcel.writeString(description);
        parcel.writeParcelable(owner,i);
    }
}
