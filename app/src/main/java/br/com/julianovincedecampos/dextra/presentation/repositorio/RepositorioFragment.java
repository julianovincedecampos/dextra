package br.com.julianovincedecampos.dextra.presentation.repositorio;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.transition.ChangeBounds;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import br.com.julianovincedecampos.dextra.R;
import br.com.julianovincedecampos.dextra.pojo.Repositories;
import br.com.julianovincedecampos.dextra.presentation.perfil.PerfilActivity;
import br.com.julianovincedecampos.dextra.presentation.recycler_view.RecyclerViewOnclickListenerHack;
import br.com.julianovincedecampos.dextra.presentation.recycler_view.RecyclerViewTouchListener;
import br.com.julianovincedecampos.dextra.util.Injection;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juliano Vince on 31/06/2017.
 */
public class RepositorioFragment extends Fragment implements RecyclerViewOnclickListenerHack, RepositorioContract.View {
    private static final String TAG_REPOSITORIO_LIST = "REPOSITORIO_LIST";

    private RepositorioContract.Presenter mPresenter;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<Repositories> mListaRepositorio;
    RepositorioAdapter repositorioAdapter = null;
    public static int PAGINA = 0;
    private Animation animationBlink;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.textViewLoading)
    TextView textViewLoading;

    @BindView(R.id.linearLayoutLoading)
    LinearLayout linearLayoutLoading;

    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_generic, container, false);
        ButterKnife.bind(this, mView);
        createView(mView);
        startAnimation(this);
        mPresenter = new RepositorioPresenter(this, Injection.provideRepositorioService(getActivity()), Injection.provideNetworkChecker(getActivity()));
        if (savedInstanceState == null) {
            mPresenter.requestBuscaRepositorio(PAGINA, repositorioAdapter, mListaRepositorio);
        }
        setHasOptionsMenu(true);
        return mView;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mListaRepositorio = savedInstanceState.getParcelableArrayList(TAG_REPOSITORIO_LIST);
            repositorioAdapter = new RepositorioAdapter(getActivity(),mListaRepositorio);
            recyclerView.setAdapter(repositorioAdapter);
            stopAnimation(this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(TAG_REPOSITORIO_LIST, mListaRepositorio);
    }

    @Override
    public void createView(View view) {
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), recyclerView, this));

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(repositorioAdapter);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.accent);
        mSwipeRefreshLayout.setOnRefreshListener(onRefreshListener());

    }

    @Override
    public void onClickListener(View view, int position) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setSharedElementEnterTransition(new ChangeBounds());
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), Pair.create(view, "button_shared"));
            Intent intent = new Intent(getActivity(), PerfilActivity.class);
            intent.putExtra("Repositories", mListaRepositorio.get(position));
            startActivity(intent, options.toBundle());
        }
    }


    @Override
    public void onLongPressClickListener(View view, int position) {
    }

    @Override
    public void createAdapter(ArrayList<Repositories> listaRepositorio) {
        if (mListaRepositorio != null) {
            mListaRepositorio.clear();
        }

        mListaRepositorio = listaRepositorio;
        repositorioAdapter = new RepositorioAdapter(getActivity(), mListaRepositorio);
        recyclerView.setAdapter(repositorioAdapter);
        stopAnimation(this);
    }

    @Override
    public void finishActivity() {

    }


    @Override
    public void errorConection() {
        Snackbar snackbar = Snackbar.make(mView, "Sem conexão com a Internet. Por favor, verifique sua WiFi ou 3G.", Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(getActivity().getResources().getColor(R.color.accent));
        snackbar.setAction("Conectar", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivity(intent);
            }
        });
        snackbar.show();
    }


    @Override
    public void showDialogMessage(String mensagem) {
        new MaterialDialog.Builder(getActivity())
                .content(mensagem)
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        getActivity().finish();
                    }
                })

                .positiveText("OK")
                .show();
    }


    @Override
    public SwipeRefreshLayout.OnRefreshListener onRefreshListener() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                repositorioAdapter = (RepositorioAdapter) recyclerView.getAdapter();
                mPresenter.requestBuscaRepositorio(PAGINA, repositorioAdapter, mListaRepositorio);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        };
    }

    @Override
    public void startAnimation(RepositorioContract.View view) {
        animationBlink = AnimationUtils.loadAnimation(getActivity(), R.anim.blink);
        textViewLoading.startAnimation(animationBlink);
    }

    @Override
    public void stopAnimation(RepositorioContract.View view) {
        textViewLoading.clearAnimation();
        linearLayoutLoading.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setRefreshing(false);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_searchable);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint(getString(R.string.search_hint_repositorio));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                queryBusca(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(final String query) {
                return true;
            }
        });
    }

    private void queryBusca(final String query) {
        if (repositorioAdapter != null) {
            repositorioAdapter.getFilter().filter(query,
                    new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int count) {
                            if (count == 0) {
                                mSwipeRefreshLayout.setRefreshing(true);
                                mPresenter.requestBuscaRepositorio(query);
                            }
                        }
                    });
        }
    }
}
