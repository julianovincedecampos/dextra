package br.com.julianovincedecampos.dextra.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Juliano.Campos on 27/01/2017.
 */

public class User implements Parcelable {
    private String login;
    private String id;
    private String avatar_url;
    private String score;


    public String getLogin() {
        return login;
    }

    public String getId() {
        return id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getScore() {
        return score;
    }

    public static final Parcelable.Creator<User>
            CREATOR = new Parcelable.Creator<User>() {

        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    public User(Parcel parcel) {
        this.login = parcel.readString();
        this.id = parcel.readString();
        this.avatar_url = parcel.readString();
        this.score = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(login);
        parcel.writeString(id);
        parcel.writeString(avatar_url);
        parcel.writeString(score);
    }
}
