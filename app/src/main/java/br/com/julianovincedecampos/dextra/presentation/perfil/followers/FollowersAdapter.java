package br.com.julianovincedecampos.dextra.presentation.perfil.followers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.julianovincedecampos.dextra.R;
import br.com.julianovincedecampos.dextra.pojo.Followers;

/**
 * Created by Juliano Vince on 01/06/2017.
 */

public class FollowersAdapter extends RecyclerView.Adapter<FollowersViewHolder>{
    private List<Followers> mFollowersList;
    private Context mContext;

    FollowersAdapter(Context pContext, List<Followers> pFollowersList) {
        mContext =pContext;
        mFollowersList = pFollowersList;
    }

    @Override
    public FollowersViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user, viewGroup, false);
        return new FollowersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FollowersViewHolder viewHolder, int position) {
        viewHolder.textViewLogin.setText(mFollowersList.get(position).getLogin());


        Picasso.with(mContext).
                load(mFollowersList.get(position).getAvatar_url())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(viewHolder.imageViewUser);
    }

    @Override
    public int getItemCount() {
        return mFollowersList.size();
    }
}
