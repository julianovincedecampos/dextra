package br.com.julianovincedecampos.dextra.presentation.perfil.followers;

import android.content.Context;

import java.util.List;

import br.com.julianovincedecampos.dextra.data.followers.FollowersService;
import br.com.julianovincedecampos.dextra.pojo.Followers;
import br.com.julianovincedecampos.dextra.util.NetworkChecker;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Juliano Vince on 01/06/2017.
 */

public class FollowersPresenter implements FollowersContract.Presenter {

    private FollowersContract.View mView;
    private Context mContext;
    private NetworkChecker mNetworkChecker;
    private FollowersService mFollowersService;

    public FollowersPresenter(FollowersContract.View view,
                              FollowersService pFollowersService,
                              Context context, NetworkChecker pNetworkChecker) {
        mView = view;
        mFollowersService = pFollowersService;
        mContext = context;
        mNetworkChecker = pNetworkChecker;
    }


    @Override
    public void requestBusca(String parametro) {
        if (mNetworkChecker.isDeviceConnected()) {
            mFollowersService.getSeguidores(parametro)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            new Subscriber<List<Followers>>() {
                                @Override
                                public void onStart() {
                                }

                                @Override
                                public void onError(Throwable e) {
                                    mView.showDialogMessage("Sobrecarga de consultas no servidor! Feche o aplicativo e tente novamente.");
                                }

                                @Override
                                public void onNext(List<Followers> lista) {
                                    mView.initRecyclerView(lista);
                                }

                                @Override
                                public void onCompleted() {
                                }
                            }
                    );
        } else {
            mView.errorConection();
        }
    }
}
