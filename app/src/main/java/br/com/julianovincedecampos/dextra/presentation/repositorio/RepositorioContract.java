package br.com.julianovincedecampos.dextra.presentation.repositorio;

import android.support.v4.widget.SwipeRefreshLayout;

import java.util.ArrayList;

import br.com.julianovincedecampos.dextra.pojo.Repositories;


/**
 * Created by Juliano Vince on 31/06/2017.
 */
public interface RepositorioContract {

    interface View{
        void createAdapter(ArrayList<Repositories> listaRepositorio);
        void finishActivity();
        void errorConection();
        void createView(android.view.View view);
        void showDialogMessage(String mensagem);
        SwipeRefreshLayout.OnRefreshListener onRefreshListener();
        void startAnimation(View view);
        void stopAnimation(View view);
    }

    interface Presenter{
        void requestBuscaRepositorio(int pagina, final RepositorioAdapter repositorioAdapter,
                                     final ArrayList<Repositories> mListaRepositorio);

        void requestBuscaRepositorio(String query);
    }
}
