package br.com.julianovincedecampos.dextra.presentation.main;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.transition.Slide;
import android.view.Menu;
import android.view.View;
import android.view.Window;

import br.com.julianovincedecampos.dextra.R;
import br.com.julianovincedecampos.dextra.util.BaseAppCompatActivity;
/**
 * Created by Juliano Vince on 29/05/2017.
 */
public class MainActivity extends BaseAppCompatActivity {
    private static final String  TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ){
            getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS );
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS );
            Slide slide = new Slide();
            slide.setDuration(3000);
        }
        super.onCreate(savedInstanceState);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new TabsAdapter(getSupportFragmentManager()));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void setUpToolbar() {
        super.setUpToolbar();
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                finish();
                }
            });
        }
    }

    @Override
    protected int provideContentViewResource() {
        return R.layout.activity_main;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}
