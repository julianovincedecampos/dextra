package br.com.julianovincedecampos.dextra.util;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;

import br.com.julianovincedecampos.dextra.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juliano Vince on 29/01/2017.
 */

public abstract class BaseAppCompatActivity extends AppCompatActivity  {
    @Nullable
    @BindView(R.id.toolbar) protected Toolbar mToolbar;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @LayoutRes
    protected abstract int provideContentViewResource();

    @Override
    protected void onCreate(@Nullable Bundle inState) {
        super.onCreate(inState);
        setContentView(provideContentViewResource());
        ButterKnife.bind(this);
        setUpToolbar();
    }

    protected void setUpToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }
}
