package br.com.julianovincedecampos.dextra.presentation.user;

import android.support.v4.widget.SwipeRefreshLayout;

import java.util.ArrayList;

import br.com.julianovincedecampos.dextra.pojo.User;


/**
 * Created by Juliano Vince on 30/05/2017.
 */
public interface UserContract {

    interface View{
        void createAdapter(ArrayList<User> listaUsers);
        void finishActivity();
        void errorConection();
        void showDialogMessage(String mensagem);
        void createView(android.view.View view);
        SwipeRefreshLayout.OnRefreshListener onRefreshListener();
        void startAnimation(UserContract.View view);
        void stopAnimation(UserContract.View view);
    }

    interface Presenter{
        void requestBuscaUser(int pagina, final UserAdapter userAdapter,
                                     final ArrayList<User> mListaUser);

        void requestBuscaUser(String query);
    }
}
