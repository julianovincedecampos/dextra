package br.com.julianovincedecampos.dextra.presentation.user;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.julianovincedecampos.dextra.R;
import br.com.julianovincedecampos.dextra.pojo.User;
import br.com.julianovincedecampos.dextra.presentation.viewholder.ViewHolderGeneric;

/**
 * Created by Juliano Vince on 30/05/2017.
 */
public class UserAdapter extends RecyclerView.Adapter<ViewHolderGeneric> implements Filterable {
    private Context mContext;
    private LayoutInflater layoutInflater;

    private List<User> mListUser;
    private List<User> originalValueUser;

    private static final String TAG = "RecyclerAdapter";
    private ListaUserFilter mFilter;


    public UserAdapter(Context pContext, List<User> pListUser) {
        mContext = pContext;
        this.layoutInflater = LayoutInflater.from(mContext);
        mListUser = pListUser;
    }


    public ViewHolderGeneric onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_user, parent, false);
        ViewHolderGeneric viewHolder = new ViewHolderGeneric(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolderGeneric viewHolder, final int position) {
        viewHolder.textViewLogin.setText(mListUser.get(position).getLogin());
        viewHolder.textViewScore.setText(String.format("Score: ")+mListUser.get(position).getScore());

        Picasso.with(mContext).
                load(mListUser.get(position).getAvatar_url())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(viewHolder.imageViewUser);

    }

    public void addListItem(User user, int position) {
        mListUser.add(user);
        notifyItemInserted(position);
    }

    public void clear() {
        int size = this.originalValueUser.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.originalValueUser.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
        this.originalValueUser = mListUser;
    }

    @Override
    public int getItemCount() {
        return mListUser == null ? 0 : mListUser.size();
    }

    private class ListaUserFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            final FilterResults results = new FilterResults();

            if (originalValueUser == null) {
                originalValueUser = new ArrayList<>(mListUser);
            }

            if (TextUtils.isEmpty(prefix)) {
                List<User> list = new ArrayList<>(originalValueUser);
                results.values = list;
                results.count = list.size();
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                List<User> values = new ArrayList<>(originalValueUser);

                final List<User> newValues = new ArrayList<>();

                for (User user : values) {
                    if (user.getLogin().toLowerCase().contains(prefixString)) {
                        newValues.add(user);
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mListUser = (List<User>) results.values;
            notifyDataSetChanged();
        }
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ListaUserFilter();
        }
        return mFilter;
    }
}

