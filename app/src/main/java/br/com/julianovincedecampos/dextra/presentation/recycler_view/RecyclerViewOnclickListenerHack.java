package br.com.julianovincedecampos.dextra.presentation.recycler_view;

import android.view.View;

/**
 * Created by Juliano Vince on 29/05/2017.
 */

public interface RecyclerViewOnclickListenerHack {
    void onClickListener(View view, int position);
    void onLongPressClickListener(View view, int position);
}