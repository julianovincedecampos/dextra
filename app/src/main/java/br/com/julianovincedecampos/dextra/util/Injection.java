package br.com.julianovincedecampos.dextra.util;

import android.content.Context;
import android.support.annotation.NonNull;

import br.com.julianovincedecampos.dextra.data.followers.FollowersService;
import br.com.julianovincedecampos.dextra.data.user.UserService;
import br.com.julianovincedecampos.dextra.data.repositorio.RepositorioService;

/**
 * Created by Juliano Vince on 29/01/2017.
 */

public class Injection {

    private static NetworkChecker sNetworkChecker;

    public static UserService provideUserService(@NonNull Context context) {
        return ServiceFactory.createService(context, UserService.class);
    }

    public static FollowersService provideFollowersService(@NonNull Context context) {
        return ServiceFactory.createService(context, FollowersService.class);
    }

    public static RepositorioService provideRepositorioService(@NonNull Context context) {
        return ServiceFactory.createService(context, RepositorioService.class);
    }

    public static NetworkChecker provideNetworkChecker(@NonNull Context context) {
        if (sNetworkChecker == null) {
            sNetworkChecker = new NetworkChecker(context);
        }
        return sNetworkChecker;
    }
}
