package br.com.julianovincedecampos.dextra.presentation.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.julianovincedecampos.dextra.presentation.user.UserFragment;
import br.com.julianovincedecampos.dextra.presentation.repositorio.RepositorioFragment;

/**
 * Created by Juliano Vince on 29/05/2017.
 */

public class TabsAdapter extends FragmentPagerAdapter {
    private String [] titulos = {"REPOSITORIOS","USUARIOS"};

    public TabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position){
            case 0:
                fragment = new RepositorioFragment();
                break;
            case 1:
                fragment = new UserFragment();
                break;
        }

        Bundle bundle = new Bundle();
        bundle.putInt("position",position);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public int getCount() {
        return titulos.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return ( titulos[position] );
    }
}
