package br.com.julianovincedecampos.dextra.presentation.perfil;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.julianovincedecampos.dextra.pojo.Repositories;
import br.com.julianovincedecampos.dextra.pojo.User;
import br.com.julianovincedecampos.dextra.presentation.perfil.descricao.DescricaoFragment;
import br.com.julianovincedecampos.dextra.presentation.perfil.followers.FollowersFragment;

/**
 * Created by Juliano Vince on 02/06/2017.
 */

public class TabsAdapter extends FragmentPagerAdapter {
    private static final int TAB_COUNT = 2;
    private Repositories mRepositories;
    private User mUser;

    TabsAdapter(FragmentManager fm, Repositories pRepositories) {
        super(fm);
        mRepositories = pRepositories;
    }

    TabsAdapter(FragmentManager fm, User pUser) {
        super(fm);
        mUser = pUser;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Override
    public Fragment getItem(int i) {
        if(mRepositories != null){
            if(i==0){
                return FollowersFragment.newInstance(mRepositories);
            }else {
                return DescricaoFragment.newInstance(mRepositories.getDescription());
            }
        }else{
            if(i==0){
                return FollowersFragment.newInstance(mUser);
            }else {
                return DescricaoFragment.newInstance("Usuario não possui descrição!");
            }
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0){
            return "SEGUIDORES";
        }else{
            return "DESCRIÇÂO";
        }

    }
}
