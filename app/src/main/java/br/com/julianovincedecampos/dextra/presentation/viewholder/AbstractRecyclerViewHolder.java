package br.com.julianovincedecampos.dextra.presentation.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by Juliano Vince on 30/05/2017.
 */
public class AbstractRecyclerViewHolder extends RecyclerView.ViewHolder {
    public AbstractRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
