package br.com.julianovincedecampos.dextra.data.followers;

import java.util.List;

import br.com.julianovincedecampos.dextra.pojo.Followers;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Juliano Vince on 29/05/2017.
 */

public interface FollowersService {
    @GET("/users/{parametro}/followers")
    Observable<List<Followers>> getSeguidores(@Path("parametro") String paramentro);
}
