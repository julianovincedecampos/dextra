package br.com.julianovincedecampos.dextra.presentation.perfil;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.julianovincedecampos.dextra.R;
import br.com.julianovincedecampos.dextra.pojo.Repositories;
import br.com.julianovincedecampos.dextra.pojo.User;
import br.com.julianovincedecampos.dextra.util.BaseAppCompatActivity;
import butterknife.BindView;
/**
 * Created by Juliano Vince on 31/06/2017.
 */
public class PerfilActivity extends BaseAppCompatActivity implements AppBarLayout.OnOffsetChangedListener {
    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
    private boolean mIsAvatarShown = true;
    private int mMaxScrollSize;

    @BindView(R.id.imageViewUser)
    ImageView imageViewUser;

    @BindView(R.id.imageViewProfile)
    ImageView imageViewProfile;



    @BindView(R.id.textViewNome)
    TextView textViewNome;

    @BindView(R.id.textViewNomeCompleto)
    TextView textViewNomeCompleto;


    @Override
    protected int provideContentViewResource() {
        return R.layout.activity_detalhes;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.materialup_tabs);
        ViewPager viewPager  = (ViewPager) findViewById(R.id.materialup_viewpager);
        AppBarLayout appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);


        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        Repositories mRepositories =  getIntent().getParcelableExtra("Repositories");

        if(mRepositories == null){
            User mUser =  getIntent().getParcelableExtra("User");
            createViews(mUser);
            viewPager.setAdapter(new TabsAdapter(getSupportFragmentManager(),mUser));
        }else{
            createViews(mRepositories);
            viewPager.setAdapter(new TabsAdapter(getSupportFragmentManager(),mRepositories));
        }


        tabLayout.setupWithViewPager(viewPager);
    }

    private void createViews(Repositories pRepositories){
        Picasso.with(this).
                load(pRepositories.getOwner().getAvatar_url())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(imageViewProfile);

        textViewNome.setText(pRepositories.getName());
        textViewNomeCompleto.setText(pRepositories.getFull_name());

        Picasso.with(this).
                load(pRepositories.getOwner().getAvatar_url())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(imageViewUser);
    }

    private void createViews(User pUser){
        Picasso.with(this).
                load(pUser.getAvatar_url())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(imageViewProfile);

        textViewNome.setText(pUser.getLogin());
        textViewNomeCompleto.setText(pUser.getScore());

        Picasso.with(this).
                load(pUser.getAvatar_url())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(imageViewUser);
    }

    @Override
    protected void setUpToolbar() {
        super.setUpToolbar();
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;

            imageViewUser.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(200)
                    .start();
        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;

            imageViewUser.animate()
                    .scaleY(1).scaleX(1)
                    .start();
        }
    }


}
