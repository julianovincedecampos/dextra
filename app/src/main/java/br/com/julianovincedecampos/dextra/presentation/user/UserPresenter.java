package br.com.julianovincedecampos.dextra.presentation.user;

import java.util.ArrayList;

import br.com.julianovincedecampos.dextra.data.user.UserService;
import br.com.julianovincedecampos.dextra.pojo.User;
import br.com.julianovincedecampos.dextra.util.NetworkChecker;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Juliano Vince on 30/05/2017.
 */
public class UserPresenter implements UserContract.Presenter {
    private static final String TAG = "<< RepositorioPresenter >>";
    private UserContract.View mView;
    private NetworkChecker mNetworkChecker;
    private UserService mUserService;


    public UserPresenter(UserContract.View view,
                         UserService userService,NetworkChecker networkChecker) {
        mView = view;
        mUserService = userService;
        mNetworkChecker = networkChecker;
    }


    public void requestBuscaUser(int pagina, final UserAdapter userAdapter, final ArrayList<User> mListaUser) {

        if (mNetworkChecker.isDeviceConnected()) {
            mUserService.getAutores(pagina)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<UserService.Autor>() {
                                   @Override
                                   public void onCompleted() {

                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       mView.showDialogMessage(e.getMessage());
                                   }

                                   @Override
                                   public void onNext(UserService.Autor autor) {
                                       handleBusca(userAdapter, autor.autores, mListaUser);
                                   }
                               }

                    );
        } else {
            mView.errorConection();
        }
    }

    @Override
    public void requestBuscaUser(String query) {
        if (mNetworkChecker.isDeviceConnected()) {
            mUserService.getAutores(query)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<UserService.Autor>() {
                                   @Override
                                   public void onCompleted() {

                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       mView.showDialogMessage(e.getMessage());
                                   }

                                   @Override
                                   public void onNext(UserService.Autor autor) {
                                       handleBusca(autor.autores);
                                   }
                               }

                    );
        } else {
            mView.errorConection();
        }
    }

    public void handleBusca(ArrayList<User> userList) {
        mView.createAdapter(userList);
        mView.stopAnimation(mView);
    }


    public void handleBusca(UserAdapter userAdapter,
                            ArrayList<User> listAux,
                            ArrayList<User> mListaUser) {

        if (userAdapter == null) {
            mView.createAdapter(listAux);
        } else {
            for (int i = 0; i < listAux.size(); i++) {
                userAdapter.addListItem(listAux.get(i), mListaUser.size());
            }
        }
        UserFragment.PAGINA++;
    }

}
