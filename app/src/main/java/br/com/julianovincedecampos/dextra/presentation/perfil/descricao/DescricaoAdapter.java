package br.com.julianovincedecampos.dextra.presentation.perfil.descricao;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.julianovincedecampos.dextra.R;

/**
 * Created by Juliano Vince on 01/06/2017.
 */

public class DescricaoAdapter extends RecyclerView.Adapter<DescricaoViewHolder> {
    private String mDescricao;

    DescricaoAdapter(String pDescricao) {
        mDescricao = pDescricao;
    }

    @Override
    public DescricaoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_descricao, viewGroup, false);
        return new DescricaoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DescricaoViewHolder viewHolder, int i) {
        viewHolder.textViewDescricao.setText(mDescricao);
    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
