package br.com.julianovincedecampos.dextra.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Juliano Vince on 29/05/2017.
 */
public class Owner implements Parcelable {
    private String login;
    private String id;
    private String avatar_url;
    private String gravatar_id;
    private String url;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getGravatar_id() {
        return gravatar_id;
    }

    public void setGravatar_id(String gravatar_id) {
        this.gravatar_id = gravatar_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Owner>
            CREATOR = new Parcelable.Creator<Owner>() {

        public Owner createFromParcel(Parcel in) {
            return new Owner(in);
        }

        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };

    public Owner() {
    }

    public Owner(String login, String id, String avatar_url, String gravatar_id, String url) {
        this.login = login;
        this.id = id;
        this.avatar_url = avatar_url;
        this.gravatar_id = gravatar_id;
        this.url = url;
    }

    public Owner(Parcel parcelable) {
        login = parcelable.readString();
        id = parcelable.readString();
        avatar_url = parcelable.readString();
        gravatar_id = parcelable.readString();
        url = parcelable.readString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(login);
        parcel.writeString(id);
        parcel.writeString(avatar_url);
        parcel.writeString(gravatar_id);
        parcel.writeString(url);
    }
}
