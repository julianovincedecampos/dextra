package br.com.julianovincedecampos.dextra.data.user;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import br.com.julianovincedecampos.dextra.pojo.User;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Juliano Vince on 29/05/2017.
 */
public interface UserService {

    @GET("/search/users?")
    Observable<UserService.Autor> getAutores(@Query("q") String q);

    @GET("/search/users?")
    Observable<UserService.Autor> getAutores(@Query("q") int page);

    final class Autor implements Serializable {
        @SerializedName("items")
        public ArrayList<User> autores;
    }
}
