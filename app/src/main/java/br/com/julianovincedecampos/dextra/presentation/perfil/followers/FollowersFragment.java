/*
 * Copyright (C) 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.julianovincedecampos.dextra.presentation.perfil.followers;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import br.com.julianovincedecampos.dextra.R;
import br.com.julianovincedecampos.dextra.pojo.Followers;
import br.com.julianovincedecampos.dextra.pojo.Repositories;
import br.com.julianovincedecampos.dextra.pojo.User;
import br.com.julianovincedecampos.dextra.util.Injection;

/**
 * Created by Juliano Vince on 01/06/2017.
 */
public class FollowersFragment extends Fragment implements FollowersContract.View {
    private RecyclerView mRootView;
    private FollowersContract.Presenter mPresenter;
    private static Repositories mRepositories;
    private static User mUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = (RecyclerView) inflater.inflate(R.layout.fragment_page, container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new FollowersPresenter(this, Injection.provideFollowersService(getActivity()), getActivity(), Injection.provideNetworkChecker(getActivity()));
        if (mRepositories != null) {
            mPresenter.requestBusca(mRepositories.getOwner().getLogin());
        } else {
            mPresenter.requestBusca(mUser.getLogin());
        }

    }

    public void initRecyclerView(List<Followers> pFollowersList) {
        mRootView.setAdapter(new FollowersAdapter(getActivity(), pFollowersList));
    }


    @Override
    public void errorConection() {
        Snackbar snackbar = Snackbar.make(getView(), "Sem conexão com a Internet. Por favor, verifique sua WiFi ou 3G.", Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(getActivity().getResources().getColor(R.color.accent));
        snackbar.setAction("Conectar", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivity(intent);
            }
        });
        snackbar.show();
    }

    @Override
    public void showDialogMessage(String mensagem) {
        new MaterialDialog.Builder(getActivity())
                .content(mensagem)
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        getActivity().finish();
                    }
                })

                .positiveText("OK")
                .show();
    }

    public static Fragment newInstance(Repositories pRepositories) {
        mRepositories = pRepositories;
        return new FollowersFragment();
    }

    public static Fragment newInstance(User pUser) {
        mUser = pUser;
        return new FollowersFragment();
    }
}
