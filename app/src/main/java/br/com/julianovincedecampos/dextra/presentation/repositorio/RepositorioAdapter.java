package br.com.julianovincedecampos.dextra.presentation.repositorio;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.julianovincedecampos.dextra.R;
import br.com.julianovincedecampos.dextra.pojo.Repositories;
import br.com.julianovincedecampos.dextra.presentation.viewholder.ViewHolderGeneric;


/**
 * Created by Juliano Vince on 31/06/2017.
 */
public class RepositorioAdapter extends RecyclerView.Adapter<ViewHolderGeneric> implements Filterable {
    private Context mContext;
    private LayoutInflater layoutInflater;
    private List<Repositories> mListRepositorio;
    private List<Repositories> originalValueRepositorio;
    private ListaRepositorioFilter mFilter;
    private static final String TAG = "RecyclerAdapter";


    public RepositorioAdapter(Context pContext, List<Repositories> listaRepositorio) {
        mContext = pContext;
        this.layoutInflater = LayoutInflater.from(pContext);
        mListRepositorio = listaRepositorio;
    }

    public ViewHolderGeneric onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_user, parent, false);
        ViewHolderGeneric viewHolder = new ViewHolderGeneric(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolderGeneric viewHolderRepositorio, final int position) {

        viewHolderRepositorio.textViewLogin.setText(mListRepositorio.get(position).getName());
        viewHolderRepositorio.textViewScore.setText(mListRepositorio.get(position).getFull_name());

        Picasso.with(mContext).
                load(mListRepositorio.get(position).getOwner().getAvatar_url())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(viewHolderRepositorio.imageViewUser);

    }

    public void addListItem(Repositories repositories, int position) {
        mListRepositorio.add(repositories);
        notifyItemInserted(position);
    }

    public void clear() {
        int size = this.mListRepositorio.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.mListRepositorio.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
        this.originalValueRepositorio = mListRepositorio;
    }

    @Override
    public int getItemCount() {
        return mListRepositorio == null ? 0 : mListRepositorio.size();
    }

    private class ListaRepositorioFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            final FilterResults results = new FilterResults();

            if (originalValueRepositorio == null) {
                originalValueRepositorio = new ArrayList<>(mListRepositorio);
            }

            if (TextUtils.isEmpty(prefix)) {
                List<Repositories> list = new ArrayList<>(originalValueRepositorio);
                results.values = list;
                results.count = list.size();
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                List<Repositories> values = new ArrayList<>(originalValueRepositorio);

                final List<Repositories> newValues = new ArrayList<>();

                for (Repositories repositories : values) {
                    if (repositories.getName().toLowerCase().contains(prefixString)) {
                        newValues.add(repositories);
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mListRepositorio = (List<Repositories>) results.values;
            notifyDataSetChanged();
        }
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ListaRepositorioFilter();
        }
        return mFilter;
    }
}
