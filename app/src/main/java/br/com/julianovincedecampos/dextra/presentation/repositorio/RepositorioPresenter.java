package br.com.julianovincedecampos.dextra.presentation.repositorio;

import java.util.ArrayList;

import br.com.julianovincedecampos.dextra.data.repositorio.RepositorioService;
import br.com.julianovincedecampos.dextra.pojo.Repositories;
import br.com.julianovincedecampos.dextra.util.NetworkChecker;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Juliano Vince on 31/06/2017.
 */
public class RepositorioPresenter implements RepositorioContract.Presenter {
    private static final String TAG = "<< RepositorioPresenter >>";
    private RepositorioContract.View mView;
    private NetworkChecker mNetworkChecker;
    private RepositorioService mRepositorioService;


    public RepositorioPresenter(RepositorioContract.View view,
                                RepositorioService repositorioService, NetworkChecker networkChecker) {
        mView = view;
        mRepositorioService = repositorioService;
        mNetworkChecker = networkChecker;
    }

    public void requestBuscaRepositorio(int pagina,
                                        final RepositorioAdapter repositorioAdapter,
                                        final ArrayList<Repositories> mListaRepositorio) {

        if (mNetworkChecker.isDeviceConnected()) {
            mRepositorioService.getRepositorios(pagina)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            new Subscriber<RepositorioService.Repositorio>() {
                                @Override
                                public void onStart() {
                                }

                                @Override
                                public void onError(Throwable e) {
                                    mView.showDialogMessage(e.getMessage());
                                }

                                @Override
                                public void onNext(RepositorioService.Repositorio lista) {
                                    handleBusca(repositorioAdapter, lista.repositorios, mListaRepositorio);
                                }

                                @Override
                                public void onCompleted() {
                                }
                            }
                    );
        } else {
            mView.showDialogMessage("Foi encontrado um problema de conexão!");
        }
    }

    public void requestBuscaRepositorio(String query) {

        if (mNetworkChecker.isDeviceConnected()) {
            mRepositorioService.getRepositorios(query)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            new Subscriber<RepositorioService.Repositorio>() {
                                @Override
                                public void onStart() {
                                }

                                @Override
                                public void onError(Throwable e) {
                                    mView.showDialogMessage("Sobrecarga de consultas no servidor! Feche o aplicativo e tente novamente.");
                                }

                                @Override
                                public void onNext(RepositorioService.Repositorio lista) {
                                    handleBusca(lista.repositorios);
                                }

                                @Override
                                public void onCompleted() {
                                }
                            }
                    );
        } else {
            mView.errorConection();
        }
    }

    public void handleBusca(ArrayList<Repositories> itemsList) {
        mView.createAdapter(itemsList);
        mView.stopAnimation(mView);
    }

    public void handleBusca(RepositorioAdapter repositorioAdapter,
                            ArrayList<Repositories> listAux,
                            ArrayList<Repositories> mListaRepositorio) {

        if (repositorioAdapter == null) {
            mView.createAdapter(listAux);
        } else {
            for (int i = 0; i < listAux.size(); i++) {
                repositorioAdapter.addListItem(listAux.get(i), mListaRepositorio.size());
            }
        }
        mView.stopAnimation(mView);
        RepositorioFragment.PAGINA++;
    }

}
