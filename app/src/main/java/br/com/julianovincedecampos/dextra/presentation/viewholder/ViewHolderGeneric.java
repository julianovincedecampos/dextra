package br.com.julianovincedecampos.dextra.presentation.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.julianovincedecampos.dextra.R;
import butterknife.BindView;

/**
 * Created by Juliano Vince on 30/05/2017.
 */
public class ViewHolderGeneric extends AbstractRecyclerViewHolder {
    @BindView(R.id.imageViewUser)
    public ImageView imageViewUser;

    @BindView(R.id.textViewLogin)
    public TextView textViewLogin;

    @BindView(R.id.textViewScore)
    public TextView textViewScore;

    public ViewHolderGeneric(View itemView) {
        super(itemView);
    }
}
