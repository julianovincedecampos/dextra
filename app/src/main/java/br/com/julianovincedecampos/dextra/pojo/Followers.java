package br.com.julianovincedecampos.dextra.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Juliano Vince on 01/06/2017.
 */

public class Followers implements Parcelable {
    private String login;
    private String avatar_url;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Followers>
            CREATOR = new Parcelable.Creator<Followers>() {

        public Followers createFromParcel(Parcel in) {
            return new Followers(in);
        }

        public Followers[] newArray(int size) {
            return new Followers[size];
        }
    };

    public Followers(String login, String avatar_url) {
        this.login = login;
        this.avatar_url = avatar_url;
    }

    public Followers(Parcel parcel) {
        this.login = parcel.readString();
        this.avatar_url = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(login);
        parcel.writeString(avatar_url);
    }
}
