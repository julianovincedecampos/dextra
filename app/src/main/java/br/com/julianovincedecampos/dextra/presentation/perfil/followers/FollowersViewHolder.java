package br.com.julianovincedecampos.dextra.presentation.perfil.followers;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.julianovincedecampos.dextra.R;
import br.com.julianovincedecampos.dextra.presentation.viewholder.AbstractRecyclerViewHolder;
import butterknife.BindView;

/**
 * Created by Juliano Vince on 01/06/2017.
 */

public class FollowersViewHolder extends AbstractRecyclerViewHolder {

    @BindView(R.id.imageViewUser)
    ImageView imageViewUser;

    @BindView(R.id.textViewLogin)
    TextView textViewLogin;

    @BindView(R.id.textViewScore)
    TextView textViewScore;

    public FollowersViewHolder(View itemView) {
        super(itemView);
    }
}
