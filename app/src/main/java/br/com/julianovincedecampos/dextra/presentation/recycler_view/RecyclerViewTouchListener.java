package br.com.julianovincedecampos.dextra.presentation.recycler_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Juliano Vince on 29/05/2017.
 */

public class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener {
    private Context mContext;
    private GestureDetector mGestureDetector;
    private RecyclerViewOnclickListenerHack mRecyclerViewOnclickListenerHack;
    private RecyclerView mRecyclerView;

    public RecyclerViewTouchListener(Context context,
                                     final RecyclerView recyclerView,
                                     RecyclerViewOnclickListenerHack recyclerViewOnclickListenerHack){
        mRecyclerView = recyclerView;
        mContext = context;
        mRecyclerViewOnclickListenerHack = recyclerViewOnclickListenerHack;

        mGestureDetector = new GestureDetector(mContext,new GestureDetector.SimpleOnGestureListener(){
            @Override
            public void onLongPress(MotionEvent e) {
                super.onLongPress(e);
                View view = mRecyclerView.findChildViewUnder(e.getX(),e.getY());
                if(view != null && mRecyclerViewOnclickListenerHack != null){
                    mRecyclerViewOnclickListenerHack.onLongPressClickListener(view,recyclerView.getChildPosition(view));
                }
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                View view = mRecyclerView.findChildViewUnder(e.getX(),e.getY());
                if(view != null && mRecyclerViewOnclickListenerHack != null){
                    mRecyclerViewOnclickListenerHack.onClickListener(view,recyclerView.getChildPosition(view));
                }

                return ( true );
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        mGestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
}

