package br.com.julianovincedecampos.dextra.presentation.user;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.transition.ChangeBounds;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import br.com.julianovincedecampos.dextra.R;
import br.com.julianovincedecampos.dextra.pojo.User;
import br.com.julianovincedecampos.dextra.presentation.perfil.PerfilActivity;
import br.com.julianovincedecampos.dextra.presentation.recycler_view.RecyclerViewOnclickListenerHack;
import br.com.julianovincedecampos.dextra.presentation.recycler_view.RecyclerViewTouchListener;
import br.com.julianovincedecampos.dextra.util.Injection;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juliano Vince on 30/05/2017.
 */
public class UserFragment extends Fragment implements RecyclerViewOnclickListenerHack, UserContract.View {
    private static final String TAG_USER_LIST = "USER_LIST";

    private UserContract.Presenter mPresenter;
    private Animation animationBlink;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<User> mListaUser;
    private UserAdapter userAdapter = null;
    public static int PAGINA = 0;
    private View mView;

     @BindView(R.id.swiperefresh)
     SwipeRefreshLayout mSwipeRefreshLayout;

     @BindView(R.id.recyclerView)
     RecyclerView recyclerView;

     @BindView(R.id.textViewLoading)
     TextView textViewLoading;

     @BindView(R.id.linearLayoutLoading)
     LinearLayout linearLayoutLoading;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_generic, container, false);
        ButterKnife.bind(this, mView);
        createView(mView);
        startAnimation(this);
        mPresenter = new UserPresenter(this, Injection.provideUserService(getActivity()), Injection.provideNetworkChecker(getActivity()));
        if (savedInstanceState == null) {
            mPresenter.requestBuscaUser(PAGINA, userAdapter, mListaUser);
        }
        setHasOptionsMenu(true);
        return mView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(TAG_USER_LIST, mListaUser);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mListaUser = savedInstanceState.getParcelableArrayList(TAG_USER_LIST);
            userAdapter = new UserAdapter(getActivity(),mListaUser);
            recyclerView.setAdapter(userAdapter);
            stopAnimation(this);
        }
    }

    @Override
    public void createView(View view) {
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), recyclerView, this));

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(userAdapter);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.accent);
        mSwipeRefreshLayout.setOnRefreshListener(onRefreshListener());
    }

    @Override
    public void onClickListener(View view, int position) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setSharedElementEnterTransition(new ChangeBounds());
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), Pair.create(view, "button_shared"));
            Intent intent = new Intent(getActivity(), PerfilActivity.class);
            intent.putExtra("User",mListaUser.get(position));
            startActivity(intent, options.toBundle());
        }
    }

    @Override
    public void onLongPressClickListener(View view, int position) {    }

    @Override
    public void createAdapter(ArrayList<User> listaUser) {
        if(mListaUser!=null){
            mListaUser.clear();
        }
        mListaUser = listaUser;
        userAdapter = new UserAdapter(getActivity(),mListaUser);
        recyclerView.setAdapter(userAdapter);
        stopAnimation(this);
    }

    @Override
    public void finishActivity() {

    }


    @Override
    public void errorConection() {
        Snackbar snackbar = Snackbar.make(mView, "Sem conexão com a Internet. Por favor, verifique sua WiFi ou 3G.", Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(getActivity().getResources().getColor(R.color.accent));
        snackbar.setAction("Conectar", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivity(intent);
            }
        });
        snackbar.show();
    }

    @Override
    public void showDialogMessage(String mensagem) {
        new MaterialDialog.Builder(getActivity())
                .content(mensagem)
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        getActivity().finish();
                    }
                })
                .positiveText("OK")
                .show();
    }


    @Override
    public SwipeRefreshLayout.OnRefreshListener onRefreshListener() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                userAdapter = (UserAdapter) recyclerView.getAdapter();
                mPresenter.requestBuscaUser(PAGINA, userAdapter, mListaUser);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        };
    }

    @Override
    public void startAnimation(UserContract.View view) {
        animationBlink = AnimationUtils.loadAnimation(getActivity(), R.anim.blink);
        textViewLoading.startAnimation(animationBlink);
    }

    @Override
    public void stopAnimation(UserContract.View view) {
        textViewLoading.clearAnimation();
        linearLayoutLoading.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_searchable);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint(getString(R.string.search_hint_user));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                queryBusca(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(final String query) {
                return true;
            }
        });
    }

    private void queryBusca(final String query) {
        if (userAdapter != null) {
            userAdapter.getFilter().filter(query,
                    new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int count) {
                            if (count == 0) {
                                mSwipeRefreshLayout.setRefreshing(true);
                                mPresenter.requestBuscaUser(query);
                            }
                        }
                    });
        }
    }
}
