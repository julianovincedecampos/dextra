package br.com.julianovincedecampos.dextra.presentation.perfil.descricao;

import android.view.View;
import android.widget.TextView;

import br.com.julianovincedecampos.dextra.R;
import br.com.julianovincedecampos.dextra.presentation.viewholder.AbstractRecyclerViewHolder;
import butterknife.BindView;

/**
 * Created by Juliano Vince on 01/06/2017.
 */

public class DescricaoViewHolder extends AbstractRecyclerViewHolder {

    @BindView(R.id.textViewDescricao)
    public TextView textViewDescricao;

    DescricaoViewHolder(View itemView) {
        super(itemView);
    }
}
