package br.com.julianovincedecampos.dextra.presentation.perfil.followers;

import java.util.List;

import br.com.julianovincedecampos.dextra.pojo.Followers;

/**
 * Created by Juliano Vince on 01/06/2017.
 */

public interface FollowersContract {
    interface View{
        void initRecyclerView(List<Followers> mFollowersList);
        void errorConection();
        public void showDialogMessage(String mensagem);
    }

    interface Presenter{
        void requestBusca(String parametro);
    }
}
